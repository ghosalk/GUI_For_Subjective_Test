
import json, io
from random import shuffle
import os
import pdb


f_4 = io.open('Exp_2.json','w', encoding = 'utf-8')

exp_2_imgs = []
bf_1 = json.load(io.open('AVA_FULL_CNN_LSTM_NOISY_vis.json','r', encoding = 'utf-8'))
bf_2 = json.load(io.open('AVA_FULL_CNN_LSTM_CLEAN_vis.json','r', encoding = 'utf-8'))
ours = json.load(io.open('AVA_FULL_LDA_CNN_LSTM_CLEAN_vis.json','r', encoding = 'utf-8'))
assert len(bf_1) == len(bf_2) == len(ours), 'inequal number of images'
#pdb.set_trace()


for i in range(len(bf_1)):
    this_img = {}
    assert bf_1[i]['image_id'] == bf_2[i]['image_id'] == ours[i]['image_id'], 'image ids not equal '
    this_img['id'] = bf_1[i]['image_id']
    this_img['sentences'] = []
    #pdb.set_trace()
    #for cat in  ['GT', 'B1', 'B2', 'Ours']:
    for cat in  ['B1', 'B2', 'Ours']:
        this_sent= {}
        this_sent['sentid'] = this_img['id'] + '_' + cat
        this_sent['imgid'] = this_img['id']
        this_sent['score'] = 0
        if cat == 'GT':
            this_sent['clean'] = bf_1[i]['ground_truths'][0]
        elif cat == 'B1':
            this_sent['clean'] = bf_1[i]['caption']
        elif cat == 'B2':
            this_sent['clean'] = bf_2[i]['caption']
        elif cat == 'Ours':
            this_sent['clean'] = ours[i]['caption']
            
        this_img['sentences'].append(this_sent)
    exp_2_imgs.append(this_img)
    
f_4.write(unicode(json.dumps({'Type': 'Exp_2','images': exp_2_imgs}, ensure_ascii=False)))

    